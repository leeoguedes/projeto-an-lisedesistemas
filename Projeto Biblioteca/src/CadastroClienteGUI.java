import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class CadastroClienteGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtCpf;
	private JTextField txtLogradouro;
	private JTextField txtBairro;
	private JTextField txtNumero;
	private JTextField txtTelefone;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroClienteGUI frame = new CadastroClienteGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroClienteGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(224, 255, 255));
		panel.setBounds(10, 11, 414, 240);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNome.setBounds(10, 11, 55, 25);
		panel.add(lblNome);
		
		JLabel lblCpf = new JLabel("Cpf:");
		lblCpf.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCpf.setBounds(10, 47, 55, 25);
		panel.add(lblCpf);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTelefone.setBounds(10, 185, 65, 25);
		panel.add(lblTelefone);
		
		txtNome = new JTextField();
		txtNome.setBounds(109, 13, 264, 25);
		panel.add(txtNome);
		txtNome.setColumns(10);
		
		txtCpf = new JTextField();
		txtCpf.setBounds(110, 49, 263, 25);
		panel.add(txtCpf);
		txtCpf.setColumns(10);
		
		JLabel lblRua = new JLabel("Logradouro:");
		lblRua.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblRua.setBounds(10, 83, 90, 25);
		panel.add(lblRua);
		
		txtLogradouro = new JTextField();
		txtLogradouro.setBounds(110, 83, 263, 25);
		panel.add(txtLogradouro);
		txtLogradouro.setColumns(10);
		
		JLabel lblBairro = new JLabel("Bairro:");
		lblBairro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblBairro.setBounds(10, 121, 65, 17);
		panel.add(lblBairro);
		
		txtBairro = new JTextField();
		txtBairro.setBounds(109, 119, 264, 25);
		panel.add(txtBairro);
		txtBairro.setColumns(10);
		
		JLabel lblNumero = new JLabel("N\u00FAmero:");
		lblNumero.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNumero.setBounds(10, 149, 65, 25);
		panel.add(lblNumero);
		
		txtNumero = new JTextField();
		txtNumero.setBounds(109, 155, 149, 25);
		panel.add(txtNumero);
		txtNumero.setColumns(10);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				
				
				
				
				
			}
		});
		btnCadastrar.setBounds(286, 156, 99, 32);
		panel.add(btnCadastrar);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(109, 187, 149, 25);
		panel.add(txtTelefone);
		txtTelefone.setColumns(10);
		
		btnNewButton = new JButton("Cancelar");
		btnNewButton.setBounds(286, 199, 99, 32);
		panel.add(btnNewButton);
	}
}
