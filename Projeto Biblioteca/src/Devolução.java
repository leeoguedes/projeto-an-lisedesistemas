import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;


public class Devolu��o extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Devolu��o frame = new Devolu��o();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Devolu��o() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(224, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCdigoDoEmprstimo = new JLabel("C\u00F3digo do empr\u00E9stimo:");
		lblCdigoDoEmprstimo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCdigoDoEmprstimo.setBounds(24, 21, 152, 14);
		contentPane.add(lblCdigoDoEmprstimo);
		
		textField = new JTextField();
		textField.setBounds(186, 20, 141, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblDataDeDevoluo = new JLabel("Data de devolu\u00E7\u00E3o:");
		lblDataDeDevoluo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDataDeDevoluo.setBounds(24, 89, 135, 14);
		contentPane.add(lblDataDeDevoluo);
		
		textField_1 = new JTextField();
		textField_1.setBounds(186, 88, 141, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton = new JButton("Voltar a tela principal");
		btnNewButton.setBounds(259, 228, 165, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblTaxa = new JLabel("Taxa:");
		lblTaxa.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTaxa.setBounds(24, 157, 135, 14);
		contentPane.add(lblTaxa);
		
		textField_2 = new JTextField();
		textField_2.setBounds(186, 156, 141, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
	}

}
