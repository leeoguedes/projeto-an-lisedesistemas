import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class CadastroLivroGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNomeLivro;
	private JTextField txtCodigo;
	private JTextField txtGenero;
	private JTextField txtAutor;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroLivroGUI frame = new CadastroLivroGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroLivroGUI() {
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(224, 255, 255));
		contentPane.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomeDoLivro = new JLabel("Nome do Livro:");
		lblNomeDoLivro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNomeDoLivro.setBounds(10, 11, 96, 14);
		contentPane.add(lblNomeDoLivro);
		
		txtNomeLivro = new JTextField();
		txtNomeLivro.setBounds(10, 36, 378, 30);
		contentPane.add(txtNomeLivro);
		txtNomeLivro.setColumns(10);
		
		JLabel lblCodigo = new JLabel("C\u00F3digo:");
		lblCodigo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCodigo.setBounds(10, 66, 72, 30);
		contentPane.add(lblCodigo);
		
		txtCodigo = new JTextField();
		txtCodigo.setBackground(Color.WHITE);
		txtCodigo.setBounds(10, 93, 378, 29);
		contentPane.add(txtCodigo);
		txtCodigo.setColumns(10);
		
		JLabel lblGenero = new JLabel("G\u00EAnero:");
		lblGenero.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblGenero.setBounds(10, 133, 82, 14);
		contentPane.add(lblGenero);
		
		txtGenero = new JTextField();
		txtGenero.setBounds(10, 156, 264, 29);
		contentPane.add(txtGenero);
		txtGenero.setColumns(10);
		
		JLabel lblAutor = new JLabel("Autor:");
		lblAutor.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAutor.setBounds(10, 196, 66, 14);
		contentPane.add(lblAutor);
		
		txtAutor = new JTextField();
		txtAutor.setBounds(10, 221, 264, 30);
		contentPane.add(txtAutor);
		txtAutor.setColumns(10);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnCadastrar.setBounds(309, 172, 104, 25);
		contentPane.add(btnCadastrar);
		
		JButton btnNewButton = new JButton("Calcelar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.setBounds(309, 222, 104, 25);
		contentPane.add(btnNewButton);
	}
}
