import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;


public class status extends JFrame {

	private JPanel contentPane;
	private JTextField txtStatus;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					status frame = new status();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public status() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 439, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(224, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtStatus = new JTextField();
		txtStatus.setBounds(98, 91, 207, 33);
		contentPane.add(txtStatus);
		txtStatus.setColumns(10);
		
		JButton btnOutraConsulta = new JButton("Outra Consulta");
		btnOutraConsulta.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnOutraConsulta.setBounds(133, 202, 142, 33);
		contentPane.add(btnOutraConsulta);
		
		JLabel lblStatus = new JLabel("Status:");
		lblStatus.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblStatus.setBounds(24, 100, 64, 14);
		contentPane.add(lblStatus);
	}
}
