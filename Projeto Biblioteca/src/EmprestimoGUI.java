import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;


public class EmprestimoGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EmprestimoGUI frame = new EmprestimoGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EmprestimoGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(224, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomeDoCliente = new JLabel("Nome do Cliente:");
		lblNomeDoCliente.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNomeDoCliente.setBounds(10, 21, 135, 14);
		contentPane.add(lblNomeDoCliente);
		
		textField = new JTextField();
		textField.setBounds(155, 18, 135, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNomeDoLivro = new JLabel("Nome do livro:");
		lblNomeDoLivro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNomeDoLivro.setBounds(10, 90, 135, 14);
		contentPane.add(lblNomeDoLivro);
		
		textField_1 = new JTextField();
		textField_1.setBounds(155, 87, 135, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("C\u00F3digo do livro:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(10, 150, 140, 14);
		contentPane.add(lblNewLabel);
		
		textField_2 = new JTextField();
		textField_2.setBounds(155, 149, 135, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblDataDoEmprstimo = new JLabel("Data do empr\u00E9stimo:");
		lblDataDoEmprstimo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDataDoEmprstimo.setBounds(10, 206, 140, 14);
		contentPane.add(lblDataDoEmprstimo);
		
		textField_3 = new JTextField();
		textField_3.setBounds(155, 205, 135, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnFinalizar = new JButton("Gerar data de devolu\u00E7\u00E3o");
		btnFinalizar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnFinalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnFinalizar.setBounds(234, 239, 190, 23);
		contentPane.add(btnFinalizar);
	}
}
