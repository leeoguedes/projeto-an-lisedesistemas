
public class CadastroLivro {
	private String NomeLivro;
	private String NomeAutor;
	private String CodLivro;
	private String Genero;

public String getNomeLivro() {
	return NomeLivro;
}
public void setNomeLivro(String nomeLivro) {
	NomeLivro = nomeLivro;
}
public String getNomeAutor() {
	return NomeAutor;
}
public void setNomeAutor(String nomeAutor) {
	NomeAutor = nomeAutor;
}
public String getCodLivro() {
	return CodLivro;
}
public void setCodLivro(String codLivro) {
	CodLivro = codLivro;
}
public String getGenero() {
	return Genero;
}
public void setGenero(String genero) {
	Genero = genero;
}

}
