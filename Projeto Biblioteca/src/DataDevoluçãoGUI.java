import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;


public class DataDevoluçãoGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DataDevoluçãoGUI frame = new DataDevoluçãoGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DataDevoluçãoGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(224, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDeverSerDevolvido = new JLabel("Dever\u00E1 ser devolvido em:");
		lblDeverSerDevolvido.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDeverSerDevolvido.setBounds(10, 99, 170, 49);
		contentPane.add(lblDeverSerDevolvido);
		
		textField = new JTextField();
		textField.setBounds(190, 111, 211, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Voltar a tela principal");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(272, 239, 162, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblCdigoDeEmprstimo = new JLabel("C\u00F3digo de Empr\u00E9stimo:");
		lblCdigoDeEmprstimo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCdigoDeEmprstimo.setBounds(10, 22, 153, 30);
		contentPane.add(lblCdigoDeEmprstimo);
		
		textField_1 = new JTextField();
		textField_1.setBounds(190, 29, 211, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
	}

}
